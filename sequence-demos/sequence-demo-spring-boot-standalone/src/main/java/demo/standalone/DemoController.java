package demo.standalone;

import io.gitee.sections.sequence.core.SequenceGenerator;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/demo")
public class DemoController {

    private SequenceGenerator sequenceGenerator;

    @GetMapping("/{key}")
    public Long getId(@PathVariable String key){
        //在单独配置了sequence.config.datasource后，将使用独立的数据库链接池在生成序列号
        return sequenceGenerator.next(key);
    }
}
