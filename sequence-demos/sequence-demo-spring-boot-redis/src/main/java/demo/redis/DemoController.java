package demo.redis;

import io.gitee.sections.sequence.core.SequenceGenerator;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/demo")
public class DemoController {

    private SequenceGenerator sequenceGenerator;

    @GetMapping("/{key}")
    public Long getId(@PathVariable String key){
        //根据yml配置，key1将使用redis缓存，key2将使用默认内存缓存
        return sequenceGenerator.next(key);
    }
}
