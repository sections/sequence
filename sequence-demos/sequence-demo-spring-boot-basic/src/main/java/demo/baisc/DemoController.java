package demo.baisc;

import io.gitee.sections.sequence.core.SequenceGenerator;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/demo")
public class DemoController {

    private SequenceGenerator sequenceGenerator;

    @GetMapping("/default")
    public Long getDefaultId(){
        //不传序列名称，则返回默认序列的值，默认序列的key是default
        return sequenceGenerator.next();
    }

    @GetMapping("/key1")
    public Long getIdOfKey1(){
        //基于application.yml中的sequence.config.sequences的配置生成序列
        return sequenceGenerator.next("key1");
    }

    @GetMapping("/{dynamicKey}")
    public Long getIdOfDynamicKey(@PathVariable String dynamicKey){
        //在没有任何配置的情况下，指定任何序列名称，都会立即动态创建该序列，并返回序列值
        return sequenceGenerator.next(dynamicKey);
    }

}
