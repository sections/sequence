package io.gitee.sections.sequence.core.cache;

import java.util.Optional;

public interface CacheProvider {
    void add(String key, Long number);
    Optional<Long> poll(String key);
    boolean support(String cacheMode);
}
