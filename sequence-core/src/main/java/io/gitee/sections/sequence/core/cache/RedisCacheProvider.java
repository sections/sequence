package io.gitee.sections.sequence.core.cache;

import org.springframework.data.redis.core.RedisTemplate;

import java.util.Objects;
import java.util.Optional;

public class RedisCacheProvider implements CacheProvider {

    private final static String KEY_PREFIX = "sequence:key:";
    private RedisTemplate<Object, Object> redisTemplate;

    public RedisCacheProvider(RedisTemplate<Object, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void add(String key, Long number) {
        redisTemplate.opsForList().rightPush(KEY_PREFIX + key, number);
    }

    @Override
    public Optional<Long> poll(String key) {
        final Object number = redisTemplate.opsForList().leftPop(KEY_PREFIX + key);
        return Objects.isNull(number) ? Optional.empty() : Optional.of((Long) number);
    }

    @Override
    public boolean support(String cacheMode) {
        return "redis".equals(cacheMode);
    }
}
