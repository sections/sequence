package io.gitee.sections.sequence.core;

public interface SequenceGenerator {
    Long next();
    Long next(String key);
}
