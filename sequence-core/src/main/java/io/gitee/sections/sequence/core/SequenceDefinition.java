package io.gitee.sections.sequence.core;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Objects;

@Getter
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = "key")
public class SequenceDefinition {
    private String key;
    private long initial = 0L;
    private int stepSize = 1;
    private int cacheSize = 50;
    private String cacheMode = "inMemory";

    public SequenceDefinition(String key) {
        this.setKey(key);
    }

    public SequenceDefinition(String key, long initial, int stepSize, int cacheSize, String cacheMode) {
        this.setKey(key);
        this.setInitial(initial);
        this.setStepSize(stepSize);
        this.setCacheSize(cacheSize);
        this.setCacheMode(cacheMode);
    }

    public void setInitial(long initial) {
        this.initial = initial;
    }

    public void setKey(String key) {
        if (Objects.isNull(key) || key.length() > 255) {
            throw new IllegalArgumentException("sequence key should not be null or longer than 255");
        } else {
            this.key = key;
        }
    }

    public void setCacheSize(int cacheSize) {
        if (cacheSize <= 0) {
            throw new IllegalArgumentException("sequence cache size should be positive");
        } else {
            this.cacheSize = cacheSize;
        }
    }

    public void setStepSize(int stepSize) {
        if (stepSize == 0) {
            throw new IllegalArgumentException("sequence step size should not be zero");
        } else {
            this.stepSize = stepSize;
        }
    }

    public void setCacheMode(String cacheMode) {
        if (Objects.isNull(cacheMode) || cacheMode.length() > 255) {
            throw new IllegalArgumentException("cache mode should not be null or longer than 255");
        } else {
            this.cacheMode = cacheMode;
        }
    }

    public SequenceDefinition copy() {
        return new SequenceDefinition(key, initial, stepSize, cacheSize, cacheMode);
    }
}
