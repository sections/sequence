package io.gitee.sections.sequence.core.dao;

import io.gitee.sections.sequence.core.SequenceDefinition;

import java.util.Optional;

public interface DataAccessor {
    Optional<SequenceDefinition> find(String key);

    void insert(SequenceDefinition definition);

    void update(SequenceDefinition definition);

    long grow(SequenceDefinition definition);

    void close();
}
