package io.gitee.sections.sequence.core.dao;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class DataSourceDataAccessor extends AbstractDatabaseAccessor implements DataAccessor{

    private DataSource dataSource;

    public DataSourceDataAccessor(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    protected Connection getConnection() throws ClassNotFoundException, SQLException {
        return dataSource.getConnection();
    }
}
