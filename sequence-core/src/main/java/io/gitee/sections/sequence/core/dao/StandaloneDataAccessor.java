package io.gitee.sections.sequence.core.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class StandaloneDataAccessor extends AbstractDatabaseAccessor implements DataAccessor {

    private DataSourceProperties db;

    public StandaloneDataAccessor(DataSourceProperties dataSourceProperties) {
        this.db = dataSourceProperties;
    }

    @Override
    protected Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName(db.getDriver());
        Connection connection = DriverManager.getConnection(db.getUrl(), db.getUsername(), db.getPassword());
        threadLocalConnection.set(connection);
        return connection;
    }
}
