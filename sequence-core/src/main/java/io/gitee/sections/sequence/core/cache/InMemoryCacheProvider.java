package io.gitee.sections.sequence.core.cache;

import java.util.*;

public class InMemoryCacheProvider implements CacheProvider {

    Map<String, Queue<Long>> pool = new HashMap<>();

    @Override
    public void add(String key, Long number) {
        Queue<Long> cache = pool.get(key);
        if (Objects.isNull(cache)) {
            synchronized (this) {
                cache = pool.get(key);
                if (Objects.isNull(cache)) {
                    cache = new LinkedList<>();
                    pool.put(key, cache);
                }
            }
        }
        cache.add(number);
    }

    @Override
    public Optional<Long> poll(String key) {
        return Optional.ofNullable(pool.get(key).poll());
    }

    @Override
    public boolean support(String cacheMode) {
        return "inMemory".equals(cacheMode);
    }
}
