package io.gitee.sections.sequence.core.impl;

import io.gitee.sections.sequence.core.SequenceDefinition;
import io.gitee.sections.sequence.core.SequenceGenerator;
import io.gitee.sections.sequence.core.cache.CacheProvider;
import io.gitee.sections.sequence.core.dao.DataAccessor;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

abstract class BaseTest {

    private DataAccessor dataAccessor;
    private List<CacheProvider> cacheProviders;

    public BaseTest(DataAccessor dataAccessor, List<CacheProvider> cacheProviders) {
        this.dataAccessor = dataAccessor;
        this.cacheProviders = cacheProviders;
    }

    /**
     * 单线程测试默认序列
     */
    @Test
    void single_thread_test_for_default_sequence() {
        final SequenceGenerator sequenceGenerator = new SequenceManager(dataAccessor, cacheProviders);
        final long start = sequenceGenerator.next();

        for (long i = start + 1; i < 500; i++) {
            final Long id = sequenceGenerator.next();
            assertEquals(i, id);
        }
    }

    /**
     * 单线程测试自定义序列
     */
    @Test
    void single_thread_test_for_customer_sequence() {
        final SequenceManager sequenceManager = new SequenceManager(dataAccessor, cacheProviders);
        final SequenceDefinition info = new SequenceDefinition("test", -5L, 3, 1, "inMemory");
        sequenceManager.load(Collections.singletonList(info));
        final long start = sequenceManager.next("test");

        for (long i = start + 3; i < 500; i += 3) {
            final Long id = sequenceManager.next("test");
            assertEquals(i, id);
        }
    }

    /**
     * 多线程测试默认序列
     */
    @Test
    void multi_thread_test_for_default_sequence() throws InterruptedException {
        final SequenceGenerator sequenceGenerator = new SequenceManager(dataAccessor, cacheProviders);
        final Long start = sequenceGenerator.next();

        final Long[] ids = multi_thread_test(sequenceGenerator, SequenceManager.DEFAULT_SEQUENCE_NAME);

        boolean result = true;
        int[] test = new int[10000];
        for (Long id : ids) {
            int index = id.intValue() - start.intValue() - 1;
            if (test[index] == 0) {
                test[index] = 1;
            } else {
                result = false;
                break;
            }
        }
        assertTrue(result);
    }

    /**
     * 多线程测试自定义序列
     */
    @Test
    void multi_thread_test_for_customer_sequence() throws InterruptedException {
        final SequenceManager sequenceManager = new SequenceManager(dataAccessor, cacheProviders);
        final SequenceDefinition info = new SequenceDefinition("test", 10000L, 1, 100, "inMemory");
        sequenceManager.load(Collections.singletonList(info));
        final Long start = sequenceManager.next("test");

        final Long[] ids = multi_thread_test(sequenceManager, "test");

        boolean result = true;
        int[] test = new int[10000];
        for (Long id : ids) {
            int index = id.intValue() - start.intValue() - 1;
            if (test[index] == 0) {
                test[index] = 1;
            } else {
                result = false;
                break;
            }
        }
        assertTrue(result);
    }

    private Long[] multi_thread_test(SequenceGenerator sequenceGenerator, String name) throws InterruptedException {
        final ExecutorService executorService = Executors.newFixedThreadPool(10);
        final ConcurrentLinkedQueue<Long> ids = new ConcurrentLinkedQueue<>();

        for (int i = 0; i < 10000; i++) {
            executorService.execute(() -> {
                final Long id = sequenceGenerator.next(name);
                ids.add(id);
                System.out.println(id);
            });
        }

        executorService.awaitTermination(10, TimeUnit.SECONDS);
        return ids.toArray(new Long[10000]);
    }
}