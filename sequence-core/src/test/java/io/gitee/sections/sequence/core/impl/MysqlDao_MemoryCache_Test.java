package io.gitee.sections.sequence.core.impl;

import io.gitee.sections.sequence.core.cache.InMemoryCacheProvider;
import io.gitee.sections.sequence.core.dao.DataAccessor;
import io.gitee.sections.sequence.core.dao.StandaloneDataAccessor;
import io.gitee.sections.sequence.core.dao.DataSourceProperties;

import java.util.Collections;

class MysqlDao_MemoryCache_Test extends BaseTest {

    MysqlDao_MemoryCache_Test() {
        super(createMySqlDataAccessor(), Collections.singletonList(new InMemoryCacheProvider()));
    }

    private static DataAccessor createMySqlDataAccessor() {
        final DataSourceProperties dataSourceProperties = new DataSourceProperties();
        dataSourceProperties.setUrl("jdbc:mysql://localhost:3306/sequence_test");
        dataSourceProperties.setUsername("root");
        dataSourceProperties.setPassword("123456");
        dataSourceProperties.setDriver("com.mysql.cj.jdbc.Driver");
        return new StandaloneDataAccessor(dataSourceProperties);
    }
}
