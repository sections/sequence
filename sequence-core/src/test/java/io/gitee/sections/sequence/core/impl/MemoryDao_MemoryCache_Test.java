package io.gitee.sections.sequence.core.impl;

import io.gitee.sections.sequence.core.cache.InMemoryCacheProvider;
import io.gitee.sections.sequence.core.dao.InMemoryDataAccessor;

import java.util.Collections;

class MemoryDao_MemoryCache_Test extends BaseTest{
    MemoryDao_MemoryCache_Test() {
        super(new InMemoryDataAccessor(), Collections.singletonList(new InMemoryCacheProvider()));
    }
}